/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 4.1
 * @version 1.1
 */

 /**
 * @file IDisplayComposer.idl
 *
 * @brief 显示合成接口声明。
 *
 * 模块包路径：ohos.hdi.display.composer.v1_1
 *
 * 引用：
 * - ohos.hdi.display.composer.v1_0.IDisplayComposer
 * - ohos.hdi.display.composer.v1_0.DisplayComposerType
 * - ohos.hdi.display.composer.v1_1.DisplayComposerType
 * - ohos.hdi.display.composer.v1_1.IModeCallback
 * - ohos.hdi.display.composer.v1_1.ISeamlessChangeCallback
 * - ohos.hdi.display.composer.v1_0.IRefreshCallback
 *
 * @since 4.1
 * @version 1.1
 */


package ohos.hdi.display.composer.v1_1;

import ohos.hdi.display.composer.v1_0.IDisplayComposer;
import ohos.hdi.display.composer.v1_0.DisplayComposerType;
import ohos.hdi.display.composer.v1_1.DisplayComposerType;
import ohos.hdi.display.composer.v1_1.IModeCallback;
import ohos.hdi.display.composer.v1_1.ISeamlessChangeCallback;
import ohos.hdi.display.composer.v1_0.IRefreshCallback;

/**
 * @brief 显示合成接口声明。
 *
 * 主要提供注册热插拔事件回调、获取显示设备能力集等功能，具体方法使用详见函数说明。
 *
 * @since 4.1
 * @version 1.1
 */
interface IDisplayComposer extends ohos.hdi.display.composer.v1_0.IDisplayComposer {
    /**
     * @brief 注册要在准备好更改帧速率时调用的回调。
     *
     * @param cb 指示用于通知图形服务已准备好更改帧速率的实例。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    RegSeamlessChangeCallback([in] ISeamlessChangeCallback cb);

    /**
     * @brief 获取显示设备支持的显示模式。
     *
     * @param devId 指示显示设备的 ID。
     * @param modes 表示有关显示设备支持的所有模式的信息向量，
     * 包括所有支持的分辨率、刷新率和 groupId。每种模式都有一个 ID，该 ID 将在以下情况下使用
     * 模式已设置或获取。有关详细信息，请参阅{@link DisplayModeInfoExt}。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    GetDisplaySupportedModesExt([in] unsigned int devId, [out] struct DisplayModeInfoExt[] modes);

    /**
     * @brief 设置显示设备的显示模式。
     *
     * @param devId 指示显示设备的 ID。
     * @param modeId 指示显示模式的 ID。设备切换到指定的显示模式
     * 此接口中的此参数。
     * @param cb 表示更改模式时要调用的回调。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    SetDisplayModeAsync([in] unsigned int devId, [in] unsigned int modeId, [in] IModeCallback cb);

    /**
     * @brief 获取当前 vblank 周期。
     * @param devId 指示显示设备的 ID。
     * @param period 指示 vblank 周期 （ns）。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    GetDisplayVBlankPeriod([in] unsigned int devId, [out] unsigned long period);

    /**
     * @brief 设置给定图层的参数，参数更改必须在此调用后完全生效。
     *
     * @param devId 指示显示设备的 ID。
     * @param layerId 指示要操作的层的 ID。
     * @param key 指示特定键。
     * @param value 指示与键对应的值。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    SetLayerPerFrameParameter([in] unsigned int devId, [in] unsigned int layerId, [in] String key, [out] byte[] value);

    /**
     * @brief 返回支持的参数键的列表
     *
     * @param keys 指示支持的参数键。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    GetSupportedLayerPerFrameParameterKey([out] String[] keys);

    /**
     * @brief 设置给定图层的参数，参数更改必须在此调用后完全生效。
     *
     * @param devId 指示显示设备的 ID。
     * @param width 指示显示设备的像素宽度。
     * @param height 指示显示设备的像素高度。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    SetDisplayOverlayResolution([in] unsigned int devId, [in] unsigned int width, [in] unsigned int height);

    /**
     * @brief 注册要在发生刷新事件时调用的回调。
     * 
     * @param cb 指示用于通知图形服务发生刷新事件的实例。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
    */
    RegRefreshCallback([in] IRefreshCallback cb);

    /**
     * @brief 获取显示设备的色域。
     *
     * @param devId 指示显示设备的 ID。
     * @param gamuts 指示有关显示设备支持的所有色域的信息的向量。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    GetDisplaySupportedColorGamuts([in] unsigned int devId, [out] struct ColorGamut[] gamuts);

    /**
     * @brief 获取显示设备的功能。
     *
     * @param devId 指示显示设备的 ID。
     * @param info 指示指向 hdr 设备支持的功能的指针。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    GetHDRCapabilityInfos([in] unsigned int devId, [out] struct HDRCapability info);
}
 /** @} */
