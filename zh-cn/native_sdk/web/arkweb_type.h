/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Web
 * @{
 *
 * @brief 提供ArkWeb在Native侧的能力，如网页刷新、执行JavaScript、注册回调等。
 * @since 12
 */
/**
 * @file arkweb_type.h
 *
 * @brief 提供ArkWeb在Native侧的公共类型定义。
 * @library libohweb.so
 * @syscap SystemCapability.Web.Webview.Core
 * @since 12
 */

#ifndef ARKWEB_TYPE_H
#define ARKWEB_TYPE_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义JavaScript Bridge数据的基础结构。
 *
 * @since 12
 */
typedef struct {
    /** 指向传输数据的指针。仅支持前端传入String和ArrayBuffer类型，其余类型会被json序列化后，以String类型传递。 */
    const uint8_t* buffer;
    /** 传输数据的长度。 */
    size_t size;
} ArkWeb_JavaScriptBridgeData;

/**
 * @brief 注入的JavaScript执行完成的回调。
 *
 * @since 12
 */
typedef void (*ArkWeb_OnJavaScriptCallback)(
    const char* webTag, const ArkWeb_JavaScriptBridgeData* data, void* userData);

/**
 * @brief Proxy方法被执行的回调。
 *
 * @since 12
 */
typedef void (*ArkWeb_OnJavaScriptProxyCallback)(
    const char* webTag, const ArkWeb_JavaScriptBridgeData* dataArray, size_t arraySize, void* userData);

/**
 * @brief 组件事件通知相关的通用回调。
 *
 * @since 12
 */
typedef void (*ArkWeb_OnComponentCallback)(const char* webTag, void* userData);

/**
 * @brief 注入的JavaScript结构体。
 *
 * @since 12
 */
typedef struct {
    /** 注入的JavaScript代码。 */
    const uint8_t* buffer;
    /** JavaScript代码长度。 */
    size_t size;
    /** JavaScript执行完成的回调。 */
    ArkWeb_OnJavaScriptCallback callback;
    /** 需要在回调中携带的自定义数据。 */
    void* userData;
} ArkWeb_JavaScriptObject;

/**
 * @brief 注入的Proxy方法通用结构体。
 *
 * @since 12
 */
typedef struct {
    /** 注入的方法名。 */
    const char* methodName;
    /** Proxy方法执行的回调。 */
    ArkWeb_OnJavaScriptProxyCallback callback;
    /** 需要在回调中携带的自定义数据。 */
    void* userData;
} ArkWeb_ProxyMethod;

/**
 * @brief 注入的Proxy对象通用结构体。
 *
 * @since 12
 */
typedef struct {
    /** 注入的对象名。 */
    const char* objName;
    /** 注入的对象携带的方法结构体数组。 */
    const ArkWeb_ProxyMethod* methodList;
    /** 方法结构体数组的长度。 */
    size_t size;
} ArkWeb_ProxyObject;

/**
 * @brief Controller相关的Native API结构体。
 *
 * @since 12
 */
typedef struct {
    /** 结构体的大小。 */
    size_t size;
    /** 注入JavaScript脚本。 */
    void (*runJavaScript)(const char* webTag, const ArkWeb_JavaScriptObject* javascriptObject);
    /** 注入JavaScript对象到window对象中，并在window对象中调用该对象的同步方法。 */
    void (*registerJavaScriptProxy)(const char* webTag, const ArkWeb_ProxyObject* proxyObject);
    /** 删除通过registerJavaScriptProxy注册到window上的指定name的应用侧JavaScript对象。 */
    void (*deleteJavaScriptRegister)(const char* webTag, const char* objName);
    /** 刷新当前网页。 */
    void (*refresh)(const char* webTag);
    /** 注入JavaScript对象到window对象中，并在window对象中调用该对象的异步方法。 */
    void (*registerAsyncJavaScriptProxy)(const char* webTag, const ArkWeb_ProxyObject* proxyObject);
} ArkWeb_ControllerAPI;

/**
 * @brief Component相关的Native API结构体。
 *
 * @since 12
 */
typedef struct {
    /** 结构体的大小。 */
    size_t size;
    /** 当Controller成功绑定到Web组件时触发该回调。 */
    void (*onControllerAttached)(const char* webTag, ArkWeb_OnComponentCallback callback, void* userData);
    /** 网页开始加载时触发该回调，且只在主frame触发，iframe或者frameset的内容加载时不会触发此回调。 */
    void (*onPageBegin)(const char* webTag, ArkWeb_OnComponentCallback callback, void* userData);
    /** 网页加载完成时触发该回调，且只在主frame触发。 */
    void (*onPageEnd)(const char* webTag, ArkWeb_OnComponentCallback callback, void* userData);
    /** 当前Web组件销毁时触发该回调。 */
    void (*onDestroy)(const char* webTag, ArkWeb_OnComponentCallback callback, void* userData);
} ArkWeb_ComponentAPI;

/**
 * @brief 检查结构体中是否存在该成员变量。
 *
 * @since 12
 */
#define ARKWEB_MEMBER_EXISTS(s, f) \
    ((intptr_t) & ((s)->f) - (intptr_t)(s) + sizeof((s)->f) <= *reinterpret_cast<size_t*>(s))

/**
 * @brief 当前结构体存在该成员变量则返回false，否则返回true。
 *
 * @since 12
 */
#define ARKWEB_MEMBER_MISSING(s, f) (!ARKWEB_MEMBER_EXISTS(s, f) || !((s)->f))

#ifdef __cplusplus
};
#endif
#endif // ARKWEB_TYPE_H
/** @} */