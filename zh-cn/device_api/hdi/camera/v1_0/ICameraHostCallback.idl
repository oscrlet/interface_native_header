/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file ICameraHostCallback.idl
 *
 * @brief ICameraHost的回调接口，提供Camera设备和闪关灯状态变化的回调函数，回调函数由调用者实现。
 *
 * 模块包路径：ohos.hdi.camera.v1_0
 *
 * 引用：ohos.hdi.camera.v1_0.Types
 *
 * @since 3.2
 * @version 1.0
 */


package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.Types;

/**
 * @brief 定义Camera设备功能回调操作。
 *
 * 设置回调接口、返回设备状态编号、闪光灯状态以及相应的事件ID。
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface ICameraHostCallback {
     /**
     * @brief 用于Camera设备状态变化时上报状态信息给调用者。
     *
     * @param cameraId 状态发生变化的Camera设备ID。
     * @param status 最新的设备状态。
     *
     * @since 3.2
     * @version 1.0
     */
    OnCameraStatus([in] String cameraId, [in] enum CameraStatus status);

     /**
     * @brief 用于在闪光灯状态变化时上报状态信息给调用者。
     *
     * @param cameraId 状态发生变化的闪关灯所绑定的Camera设备ID。
     * @param status 最新的闪光灯状态。状态值查看{@link CameraStatus}。
     *
     * @since 3.2
     * @version 1.0
     */
    OnFlashlightStatus([in] String cameraId, [in] enum FlashlightStatus status);

     /**
     * @brief 在相机事件发生时调用。
     *
     * @param cameraId 表示相机事件绑定的相机ID。
     * @param event 表示相机事件类型。事件类型查看{@link CameraEvent}。
     *
     * @since 3.2
     * @version 1.0
     */
    OnCameraEvent([in] String cameraId, [in] enum CameraEvent event);
}
/** @} */