/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Defines audio-related APIs, including data types and functions for loading drivers, accessing a driver adapter, and rendering and capturing audios.
 *
 *
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IAudioCallback.idl
 *
 * @brief Defines the callbacks for audio rendering.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the audio APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.audio.v1_0;

import ohos.hdi.audio.v1_0.AudioTypes;

/**
 * @brief Defines the audio-related callbacks.
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface IAudioCallback {

    /**
     * @brief Called for an audio rendering event.
     *
     * @param type Indicates the type of the callback notification. For details, see {@link AudioCallbackType}.
     * @param reserved Indicates a reserved field.
     * @param cookie Indicates the cookie for data transmission.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see RegCallback
     *
     * @since 3.2
     * @version 1.0
     */
    RenderCallback([in] enum AudioCallbackType type, [out] byte reserved, [out] byte cookie);
    /**
     * @brief Called for extended parameters.
     *
     * @param key Indicates the key of the extended parameter. For details, see {@link AudioExtParamKey}.
     * @param condition Indicates the query condition of the extended parameter.
     * @param value Indicates the condition value of the extended parameter.
      * @param reserved Indicates a reserved field.
     * @param cookie Indicates the cookie for data transmission.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see ParamCallback
     *
     * @since 3.2
     * @version 1.0
     */
    ParamCallback([in] enum AudioExtParamKey key, [in] byte condition, [in] byte value, [out] byte reserved, [out] byte cookie);
}
/** @} */
