/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup VideoEncoder
 * @{
 * 
 * @brief VideoEncoder模块提供用于视频编码的函数。
 * 
 * @syscap SystemCapability.Multimedia.VideoEncoder
 * @since 9
 */


/**
 * @file native_avcodec_videoencoder.h
 * 
 * @brief 声明用于视频编码的Native API。
 * 
 * @library libnative_media_venc.so
 * @since 9
 */

#ifndef NATIVE_AVCODEC_VIDEOENCODER_H
#define NATIVE_AVCODEC_VIDEOENCODER_H

#include <stdint.h>
#include <stdio.h>
#include "native_avcodec_base.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 当OH_AVCodec在运行过程中需要新的输入参数时，将调用此函数指针，并携带可用的缓冲区来填充新的输入参数。设置的参数随帧立即生效。
 *
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 与新可用的输入参数缓冲区相对应的索引。
 * @param parameter  新的可用输入参数缓冲区。
 * @param userData 用户特定数据。
 * @since 12
 */
typedef void (*OH_VideoEncoder_OnNeedInputParameter)(OH_AVCodec *codec, uint32_t index, OH_AVFormat *parameter,
                                                     void *userData);
/**
 * @brief 从MIME类型创建视频编码器实例，大多数情况下建议使用。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param mime MIME类型描述字符串，请参阅{@link AVCODEC_MIME_TYPE}。
 * @return 成功则返回一个指向视频编码实例的指针。
 * 如果输入为不支持的编码器类型或内存不足时，则返回nullptr。
 * @since 9
 * @version 1.0
 */
OH_AVCodec *OH_VideoEncoder_CreateByMime(const char *mime);

/**
 * @brief 通过视频编码器名称创建视频编码器实例。使用此接口的前提是知道编码器的确切名称。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param name 视频编码器名称。
 * @return 成功则返回一个指向视频编码实例的指针。
 * 如果输入是不支持编码器名称或者内存资源不足，则返回nullptr。
 * @since 9
 * @version 1.0
 */
OH_AVCodec *OH_VideoEncoder_CreateByName(const char *name);

/**
 * @brief 清理编码器内部资源，销毁编码器实例。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当编码器实例已经销毁，返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Destroy(OH_AVCodec *codec);

/**
 * @brief 设置异步回调函数，以便您的应用程序可以响应视频编码器生成的事件。在调用Prepare之前，必须调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param callback 所有回调函数的集合，请参阅{@link OH_AVCodecAsyncCallback}。
 * @param userData 用户特定数据。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 本接口必须在Prepare接口前调用，如果在其他状态时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @deprecated since 11
 * @useinstead OH_VideoEncoder_RegisterCallback
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_SetCallback(OH_AVCodec *codec, OH_AVCodecAsyncCallback callback, void *userData);

/**
 * @brief 注册异步回调函数，以便您的应用程序可以响应视频编码器生成的事件。在调用Prepare之前，必须调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param callback 所有回调函数的集合，请参阅{@link OH_AVCodecCallback}。
 * @param userData 用户特定数据。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 本接口必须在Prepare接口前调用，如果在其他状态时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 11
 */
OH_AVErrCode OH_VideoEncoder_RegisterCallback(OH_AVCodec *codec, OH_AVCodecCallback callback, void *userData);

/**
 * @brief 注册异步输入参数回调函数，以便您的应用程序可以响应视频编码器生成的事件。编码surface模式，需要设置随帧参数时，须使用该接口。
 * 如果使用该接口，必须在{@link OH_VideoEncoder_Configure}之前调用该接口。
 *
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param onInputParameter 输入参数回调指针, 请参阅{@link OH_VideoEncoder_OnNeedInputParameter}。
 * @param userData 用户特定数据。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 本接口必须在Prepare接口前调用，如果在其他状态时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 12
 */
OH_AVErrCode OH_VideoEncoder_RegisterParameterCallback(OH_AVCodec *codec,
                                                       OH_VideoEncoder_OnNeedInputParameter onInputParameter,
                                                       void *userData);

/**
 * @brief 配置视频编码器，通常需要配置要编码的视频轨的描述信息。必须在调用Prepare之前，调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param format 指向OH_AVFormat的指针，用于给出要编码的视频轨的描述。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，或输入format参数不支持,返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 本接口必须在Prepare接口前调用，如果在其他状态时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Configure(OH_AVCodec *codec, OH_AVFormat *format);

/**
 * @brief 若要准备编码器的内部资源，必须先调用Configure接口，再调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Prepare(OH_AVCodec *codec);

/**
 * @brief 启动编码器，准备成功后必须调用此接口。成功启动后，编码器将开始报告注册的回调事件。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Start(OH_AVCodec *codec);

/**
 * @brief 停止编码器。停止之后，你可以通过Start接口进入Started状态。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Stop(OH_AVCodec *codec);

/**
 * @brief 清除编码器中缓存的输入和输出数据。
 * 
 * 调用此接口后，以前通过异步回调上报的所有缓冲区索引都将失效，请确保不要访问这些索引对应的缓冲区。
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Flush(OH_AVCodec *codec);

/**
 * @brief 重置编码器。如果要继续编码，需要再次调用Configure接口配置编码器实例。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Reset(OH_AVCodec *codec);

/**
 * @brief 获取编码器输出数据的描述信息，请参阅{@link OH_AVFormat}。
 * 
 * 需要注意的是，返回值指向的OH_AVFormat实例的生命周期需要调用者手动释放。
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回指向OH_AVFormat实例的指针。
 * 当输入的codec指针非编码实例，或者为空指针，则返回空指针。
 * @since 9
 * @version 1.0
 */
OH_AVFormat *OH_VideoEncoder_GetOutputDescription(OH_AVCodec *codec);

/**
 * @brief 为编码器设置动态参数。
 * 注意，此接口只有在编码器启动后才能调用。 同时，不正确的参数设置可能会导致编码失败。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param format 指向OH_AVFormat实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，或输入format参数不支持,返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_SetParameter(OH_AVCodec *codec, OH_AVFormat *format);

/**
 * @brief 从视频编码器获取输入Surface，必须在调用Prepare之前调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param window 指向OHNativeWindow实例的指针, 请参阅{@link OHNativeWindow}。
 * 应用负责管理window的生命周期，结束时调用{@link OH_NativeWindow_DestroyNativeWindow}释放。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_GetSurface(OH_AVCodec *codec, OHNativeWindow **window);

/**
 * @brief 将处理后的输出缓冲区返回给编码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 输出缓冲区对应的索引值。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * index由{@link OH_AVCodecOnNewOutputData}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @deprecated since 11
 * @useinstead OH_VideoEncoder_FreeOutputBuffer
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_FreeOutputData(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 通知视频编码器输入流已结束。建议使用此接口进行通知。编码器surface模式下，输入流的结束通知。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_NotifyEndOfStream(OH_AVCodec *codec);

/**
 * @brief 将填入数据的输入缓冲区提交给视频编码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 输入缓冲区对应的索引值。
 * @param attr 缓冲区中包含数据的描述信息。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * index由{@link OH_AVCodecOnNeedInputData}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @deprecated since 11
 * @useinstead OH_VideoEncoder_PushInputBuffer
 * @since 10
 */
OH_AVErrCode OH_VideoEncoder_PushInputData(OH_AVCodec *codec, uint32_t index, OH_AVCodecBufferAttr attr);

/**
 * @brief 将填入数据的输入缓冲区提交给视频编码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 输入缓冲区对应的索引值。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，或输入format参数不支持，返回{@link AV_ERR_INVALID_VAL}。
 * index由{@link OH_AVCodecOnNeedInputBuffer}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 11
 */
OH_AVErrCode OH_VideoEncoder_PushInputBuffer(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 将填入数据的输入参数缓冲区提交给视频编码器。
 *
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 输入参数缓冲区对应的索引值。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * index由{@link OH_VideoEncoder_OnNeedInputParameter}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 12
 */
OH_AVErrCode OH_VideoEncoder_PushInputParameter(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 将处理后的输出缓冲区返回给编码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 输出缓冲区对应的索引值。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，或输入format参数不支持，返回{@link AV_ERR_INVALID_VAL}。
 * index该由{@link OH_AVCodecOnNewOutputBuffer}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当编码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 11
 */
OH_AVErrCode OH_VideoEncoder_FreeOutputBuffer(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 获取视频编码器接收的描述信息，调用{@OH_VideoEncoder_Configure}后调用此接口，请参阅{@link OH_AVFormat}获取详细信息。
 * 应该注意的是，返回指针所指向的OH_AVFormat实例的生命周期需要由调用者手动释放，请参阅{@link OH_AVFormat_Destory}。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回指向OH_AVFormat实例的指针。
 * 当codec指针非编码实例，或者为空指针，则返回空指针。
 * @since 10
 */
OH_AVFormat *OH_VideoEncoder_GetInputDescription(OH_AVCodec *codec);

/**
 * @brief 检查当前编码实例是否有效。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param isValid 输出参数。指向布尔实例的指针，如果编码器实例有效，则为true，如果编码器实例无效，则为false。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的编码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非编码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * @since 10
 */
OH_AVErrCode OH_VideoEncoder_IsValid(OH_AVCodec *codec, bool *isValid);

/**
 * @brief 视频编码器的比特率模式。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @since 9
 * @version 1.0
 */
typedef enum OH_VideoEncodeBitrateMode {
    /** 恒定比特率模式。 */
    CBR = 0,
    /** 可变比特率模式。 */
    VBR = 1,
    /** 恒定质量模式。 */
    CQ = 2,
} OH_VideoEncodeBitrateMode;

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVCODEC_VIDEOENCODER_H

/** @} */