/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief 定义上层WLAN服务的API接口.
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点
 * WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 3.2
 * @version 1.1
 */

 /**
 * @file WlanTypes.idl
 *
 * @brief 定义与WLAN模块相关的数据类型.
 *
 * WLAN模块数据包括{@code-Feature}对象信息、站（STA）信息等,扫描信息和网络设备信息
 *
 * 模块包路径：ohos.hdi.wlan.v1_1
 *
 * @since 3.2
 * @version 1.1
 */

package ohos.hdi.wlan.v1_1;

/**
 * @brief 定义{@code Feature}对象信息。
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfFeatureInfo {
    /** Feature对象的网卡名称 */
    String ifName;
    /** Feature对象类型 */
    int type;
};

/**
 * @brief 定义STA信息。
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfStaInfo {
    /** STA的MAC地址 */
    unsigned char[] mac;
};

/**
 * @brief 定义Wi-Fi扫描的服务集标识符（SSID）信息。
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfWifiDriverScanSsid {
    /** WiFi扫描的SSID */
    String ssid;
    /** SSID长度 */
    int ssidLen;
};

/**
 * @brief 定义Wi-Fi扫描参数。
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfWifiScan{
    /** WiFi扫描的SSID集合 */
    struct HdfWifiDriverScanSsid[] ssids;
    /** WiFi扫描的频率集合 */
    int[] freqs;
    /** WiFi扫描请求中携带的扩展IE */
    unsigned char[] extraIes;
    /** WiFi扫描的BSSID */
    unsigned char[] bssid;
    /** SSID扫描的前缀标志 */
    unsigned char prefixSsidScanFlag;
    /** 快速连接标志 */
    unsigned char fastConnectFlag;
};

/**
 * @brief 定义网络设备信息。
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfNetDeviceInfo {
    /** 网络设备索引 */
    unsigned int index;
    /** 网卡名称 */
    String ifName;
    /** 网卡名称长度 */
    unsigned int ifNameLen;
    /** 网卡类型 */
    unsigned char iftype;
    /** 网络设备MAC地址 */
    unsigned char[] mac;
};

/**
 * @brief 定义网络设备信息集。
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfNetDeviceInfoResult {
    /** 网络设备信息集合 */
    struct HdfNetDeviceInfo[] deviceInfos;
};

/**
 * @brief 定义Wi-Fi扫描结果。
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfWifiScanResult {
    /** BSS/IBSS的标志位 */
    unsigned int flags;
    /** BSSID信息 */
    unsigned char[] bssid;
    /** Capability信息字段（主机字节序排列） */
    unsigned short caps;
    /** 信道频率 */
    unsigned int freq;
    /** Beacon帧间隔 */
    unsigned short beaconInt;
    /** 信号质量 */
    int qual;
    /** 信号强度 */
    int level;
    /** 收到最新的Beacon或者探测响应帧数据的时间长度，单位为毫秒。 */
    unsigned int age;
    /** 扫描结果中的变量值 */
    unsigned char[] variable;
    /** 紧跟的Probe Response中IE字段 */
    unsigned char[] ie;
    /** 紧跟的Beacon中IE字段 */
    unsigned char[] beaconIe;
};

/**
 * @brief 定义Wi-Fi扫描结果。
 *
 * @since 4.0
 * @version 1.1
 */
struct HdfWifiScanResultExt {
    /** BSS/IBSS的标志位 */
    unsigned int flags;
    /** BSSID信息 */
    unsigned char[] bssid;
    /** Capability信息字段（主机字节序排列） */
    unsigned short caps;
    /** 信道频率 */
    unsigned int freq;
    /** Beacon帧间隔  */
    unsigned short beaconInt;
    /** 信号质量 */
    int qual;
    /** 信号强度 */
    int level;
    /** 收到最新的Beacon或者探测响应帧数据的时间长度，单位为毫秒。 */
    unsigned int age;
    /** 时间戳 **/
    unsigned long tsf;
    /** 扫描结果中的变量值 */
    unsigned char[] variable;
    /** 紧跟的Probe Response中IE字段 */
    unsigned char[] ie;
    /** 紧跟的Beacon中IE字段 */
    unsigned char[] beaconIe;
};

/**
 * @brief 定义Wi-Fi扫描结果。
 *
 * @since 4.0
 * @version 1.1
 */
struct HdfWifiScanResults {
    /** Wi-Fi扫描扩展结果  */
    struct HdfWifiScanResultExt[] res;
};

/**
 * @brief 定义Wi-Fi频带信息。
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfWifiInfo {
    /** Wi-Fi频段 */
    int band;
    /** Wi-Fi频段下支持的频率个数 */
    unsigned int size;
};

/**
 * @brief 定义通道测量参数。
 *
 * @since 3.2
 * @version 1.1
 */
struct MeasChannelParam {
    /** 信道号 */
    int channelId;
    /** 测量时间 */
    int measTime;
};

/**
 * @brief 定义通道测量结果。
 *
 * @since 3.2
 * @version 1.1
 */
struct MeasChannelResult {
    /** 信道号 */
    int channelId;
    /** 信道负载 */
    int chload;
    /** 信道噪声 */
    int noise;
};

/**
 * @brief 定义投影参数。
 *
 * @since 3.2
 * @version 1.1
 */
struct ProjectionScreenCmdParam {
    /** 投屏命令ID */
    int cmdId;
    /** 投屏命令内容 */
    byte[] buf;
};

/**
 * @brief 定义STA信息。
 *
 * @since 3.2
 * @version 1.1
 */
struct WifiStationInfo {
    /** 接收速率。 */
    unsigned int rxRate;
    /** 发送速率。 */
    unsigned int txRate;
    /** 速率传输类型。 */
    unsigned int flags;
    /** 接收VHT-MCS（Very High Throughput Modulation and Coding Scheme）配置。 */
    unsigned char rxVhtmcs;
    /** 发送VHT-MCS（Very High Throughput Modulation and Coding Scheme）配置。 */
    unsigned char txVhtmcs;
    /** 接收MCS（Modulation and Coding Scheme）索引。 */
    unsigned char rxMcs;
    /** 发送MCS（Modulation and Coding Scheme）索引。 */
    unsigned char txMcs;
    /** 接收VHT-NSS（Very High Throughput Number of Spatial Streams）配置。 */
    unsigned char rxVhtNss;
    /** 发送VHT-NSS（Very High Throughput Number of Spatial Streams）配置。 */
    unsigned char txVhtNss;
};

struct AdjustChannelInfo {
    int msgId;
    unsigned char chanNumber;
    unsigned char bandwidth;
    unsigned char switchType;
    unsigned char statusCode;
};

/**
 * @brief 定义Pno扫描网络信息。
 *
 * @since 4.0
 * @version 1.1
 */
struct PnoNetwork {
    /** 是否扫描隐藏网络 */
    boolean isHidden;
    /** 扫描频率 */
    int[] freqs;
    /** 扫描SSID */
    struct HdfWifiDriverScanSsid ssid;
};

/**
 * @brief 定义Pno扫描参数。
 *
 * @since 4.0
 * @version 1.1
 */
struct PnoSettings {
    /** 要扫描的最小2G Rssi */
    int min2gRssi;
    /** 要扫描的最小5G Rssi */
    int min5gRssi;
    /** 扫描间隔 */
    int scanIntervalMs;
    /** 扫描迭代 */
    int scanIterations;
    /** 扫描网络列表 */
    struct PnoNetwork[] pnoNetworks;
};

/**
 * @brief 定义信号轮询信息。
 *
 * @since 4.0
 * @version 1.1
 */
struct SignalPollResult {
    /** Rssi value in dBM. */
    int currentRssi;
    /** Association frequency in MHz. */
    int associatedFreq;
    /** Transmission bit rate in Mbps. */
    int txBitrate;
    /** Received bit rate in Mbps. */
    int rxBitrate;
    /** Noise value in dBM. */
    int currentNoise;
    /** Snr value in dB. */
    int currentSnr;
    /** Current channel load. */
    int currentChload;
    /** Uldelay value in ms. */
    int currentUlDelay;
    /** TxBytes value. */
    int currentTxBytes;
    /** RxBytes value. */
    int currentRxBytes;
    /** TxFailed value. */
    int currentTxFailed;
    /** TxPackets value. */
    int currentTxPackets;
    /** RxPackets value. */
    int currentRxPackets;
};
/** @} */