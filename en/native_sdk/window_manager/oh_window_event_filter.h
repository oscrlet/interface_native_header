/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef INCLUDE_OH_WINDOW_EVENT_FILTER_H
#define INCLUDE_OH_WINDOW_EVENT_FILTER_H


/**
 * @addtogroup WindowManager_NativeMoudle
 * @{
 *
 *
 * @brief Provides the capabilities of managing application windows.
 *
 * @since 12
 */

/**
 * @file oh_window_event_filter.h
 *
 * @brief Declares the APIs for a window to filter multimodal key events.
 * When a multimodal input event passes through the window, the window can interrupt the event and prevent it from
 * being further distributed.
 *
 * @syscap SystemCapability.Window.SessionManager
 * File to include: <window_manager/oh_window_event_filter.h>
 * @library libnative_window_manager.so
 * @since 12
 */
#include "stdint.h"
#include "oh_window_comm.h"
#include "multimodalinput/oh_input_manager.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines a function for filtering a multimodal key event.
 * @param keyEvent Multimodal key event. For details, see {@link Input_KeyEvent}.
 * The event is defined in <b>oh_input_manager</b>.
 * @return Returns <b>true</b> if the event will be intercepted; returns <b>false</b> otherwise.
 * @since 12
 */
typedef bool (*OH_NativeWindowManager_KeyEventFilter)(Input_KeyEvent* keyEvent);

/**
 * @brief Registers the function for filtering a multimodal key event.
 *
 * @param windowId ID of the window for which the function is registered.
 * @param keyEventFilter Function for filtering a multimodal key event.
 * @return Returns a status code defined in {@link WindowManager_ErrorCode}.
 * @since 12
 */
WindowManager_ErrorCode OH_NativeWindowManager_RegisterKeyEventFilter(int32_t windowId,
    OH_NativeWindowManager_KeyEventFilter keyEventFilter);

/**
 * @brief Unregisters the function for filtering a multimodal key event.
 *
 * @param windowId ID of the window for which the function is unregistered.
 * @return Returns a status code defined in {@link WindowManager_ErrorCode}.
 * @since 12
 */
WindowManager_ErrorCode OH_NativeWindowManager_UnregisterKeyEventFilter(int32_t windowId);

#ifdef __cplusplus
}
#endif

#endif // INCLUDE_OH_WINDOW_EVENT_FILTER_H
